@if($ec->group('Achtergrond')->get('background'))
<div class="s-bg" style="background-image: url('{{ $ec->group('Achtergrond')->get('background') }}'); opacity: {{ $ec->group('Achtergrond')->get('background-opacity') || 1 }}"></div>
@endif

<div class="site-wrapper">
    @if($tc->group('Site')->get('general', 'logo'))
    <header class="s-header">
        <div class="s-logo-container">
            <img src="{{ strip_tags($tc->group('Site')->get('general', 'logo')) }}" alt="{{ strip_tags($tc->group('Site')->get('general', 'site-name')) }}" class="s-logo">
        </div>
    </header>
    @endif

    <div class="c-container">
        @if($ec->group('Inleiding')->get('introduction'))
        <div class="s-spacer s-spacer--default"></div>
        <div class="s-introduction">
            {!! $ec->group('Inleiding')->get('introduction') !!}
        </div>
        @endif
        <div class="s-spacer s-spacer--small"></div>
        <hr class="s-divider">
        <div class="s-spacer s-spacer--small"></div>
        <div class="s-contact">
            <ul class="list-unstyled">
                <li><a class="s-link" target="_blank" href="{{ $ec->exclude()->get('google-maps-url') }}">{{ strip_tags($tc->group('Contactgegevens')->get('general', 'address')) }}, {{ strip_tags($tc->group('Contactgegevens')->get('general', 'postal-code')) }} {{ strip_tags($tc->group('Contactgegevens')->get('general', 'city')) }}</a></li>
                <li><span class="s-bold">T</span> <a class="s-link" href="tel:{{ str_replace(' ', '', strip_tags($tc->group('Contactgegevens')->get('general', 'phone'))) }}">{{ strip_tags($tc->group('Contactgegevens')->get('general', 'phone')) }}</a></li>
                <li><span class="s-bold">E</span> <a class="s-link" href="mailto:{{ strip_tags($tc->group('Contactgegevens')->get('general', 'email')) }}">{{ strip_tags($tc->group('Contactgegevens')->get('general', 'email')) }}</a></li>
                <li><span class="s-icon s-icon--facebook"></span> <a class="s-link" href="{{ strip_tags($tc->group('Contactgegevens')->get('social-media', 'facebook')) }}">Facebook</a></li>
            </ul>
        </div>
        <div class="s-spacer s-spacer--default"></div>
        <div class="s-hours">
            <h2 class="s-hours__title c-title c-title--bold">{{ $ec->group('Openingsuren')->get('hours-title') }}</h2>
            <ul class="s-hours__list list-unstyled">
                <li class="s-hours__block">
                    <div class="s-hours__subtitle c-title c-title--bold">
                        maa - don
                    </div>
                    <ul class="list-unstyled">
                        <li class="s-hours__note">{{ $ec->group('Openingsuren')->get('hours-1') }}</li>
                    </ul>
                </li>
                <li class="s-hours__block">
                    <div class="s-hours__subtitle c-title c-title--bold">
                        vrij
                    </div>
                    <ul class="list-unstyled">
                        <li>{{ $ec->group('Openingsuren')->get('hours-2') }}</li>
                    </ul>
                </li>
                <li class="s-hours__block">
                    <div class="s-hours__subtitle c-title c-title--bold">
                        zat - zon
                    </div>
                    <ul class="list-unstyled">
                        <li>{{ $ec->group('Openingsuren')->get('hours-3') }}</li>
                    </ul>
                </li>
            </ul>
            <div class="s-hours__notes">
                <ul class="list-unstyled">
                    <li>
                        <span class="s-important">*</span> belangrijke notitie
                    </li>
                </ul>
            </div>
        </div>
        <div class="s-spacer s-spacer--large"></div>
    </div>

    <footer class="s-footer">
        site by <a class="s-link s-link--blank" href="http://www.skinn.be">skinn</a>
    </footer>
</div>
