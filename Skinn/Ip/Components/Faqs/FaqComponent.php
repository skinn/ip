<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 06/06/2017
 * Time: 09:03
 */

namespace Skinn\Ip\Components\Faqs;


use Skinn\Ip\Components\Core\Component;

class FaqComponent extends Component
{
    protected $type = 'faqs';

    protected $data = [
        'questions' => [],
        'multiple' => false,
    ];


}
