<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 07/06/2017
 * Time: 09:44
 */

namespace Skinn\Ip\Components\Core;


class ComponentCollectionMapper
{
    public function get(){
        return $this;
    }


    public function map($data, $mapping){
        $key = key($data);
        if(is_numeric($key))
            return $this->mapCollection($data, $mapping);

        return $this->mapRecord($data, $mapping);

    }
    public function mapCollection($collection, $mapping){
        $result = [];
       foreach($collection as $index => $record)
            $result[] = $this->mapRecord($record, $mapping);

       return $result;
    }

    public function mapRecord($record, $mapping){

        $result = [];
        foreach($mapping as $target => $source)
            if(is_array($source))
                foreach($source as $item)
                    $result[$target][] = $this->getValue($record, $item);
            else
                $result[$target] = $this->getValue($record, $source);
        if(isset($result['images']) && ! is_array($result['images']))
            $result['images'] = [$result['images']];

        return $result;
    }

    /**
     * @param $record
     * @param $source
     * @return bool
     */
    private function getValue($record, $source)
    {
        $segments = explode('.', $source);
        if (count($segments) < 2)
            return '';

        if( ! isset($record[$segments[0]][$segments[1]]))
            return '';

        if(count($segments) ==  3 && ! isset($record[$segments[0]][$segments[1]][$segments[2]]))
            return [];

        if(count($segments) ==  3 && $segments[1] == 'images')
            return $this->decorateWithAssets($record['with']['files'], $record[$segments[0]][$segments[1]][$segments[2]]);

        if(count($segments) ==  4 && ! isset($record[$segments[0]][$segments[1]][$segments[2]][$segments[3]]))
            return [];

        if(count($segments) ==  4 && $segments[1] == 'images')
            return $this->decorateWithAssets($record['with']['files'], $record[$segments[0]][$segments[1]][$segments[2]][$segments[3]]);

        if($segments[1] == 'images') {
            $ids = [];
            foreach ($record[$segments[0]][$segments[1]] as $key => $value)

                $ids = array_merge($ids, $value);

            return $this->decorateWithAssets($record['with']['files'], $ids);
        }//endif

        return $record[$segments[0]][$segments[1]];
    }

    public function decorateWithAssets($assets, $ids){

        if(  ! is_array($ids))
            return $assets[$ids]['virtual_file'];

        $result = [];
        foreach($ids as $id)
            if(isset($assets[$id]))
                $result[] = $assets[$id]['virtual_file'];

        return $result;
    }
}