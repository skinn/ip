<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 31/05/2017
 * Time: 09:06
 */

namespace Skinn\Ip\Components\Core\Facades;


use Illuminate\Support\Facades\Facade;

class ComponentCollectionMapper extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return \Skinn\Ip\Components\Core\ComponentCollectionMapper::class; }
}
