<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 31/05/2017
 * Time: 15:29
 */

namespace Skinn\Ip\Components\Core;


class ComponentScriptContainer
{
    protected $js = [];

    public function registerJs($js){
        $this->js[md5($js)] = $js;
    }

    public function getScripts(){
        return $this->js;
    }
}