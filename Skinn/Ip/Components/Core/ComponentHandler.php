<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 31/05/2017
 * Time: 09:04
 */

namespace Skinn\Ip\Components\Core;


use Illuminate\Support\Facades\App;

class ComponentHandler
{
    private $classMap = [];

    public function __construct($classMap){
        $this->classMap = $classMap;
    }

    public function get($name){

        $segments = explode('.', $name);
        if(count($segments) < 2)
            throw new \Exception('in correct component name');

        if( ! isset($this->classMap[$segments[0]]))
            throw new \Exception('Component is not registered. Please check in SkinnIpServiceProvider.');

        return App::make($this->classMap[$segments[0]])->get($segments[1]);

    }
}