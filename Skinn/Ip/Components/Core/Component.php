<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 30/05/2017
 * Time: 15:01
 */

namespace Skinn\Ip\Components\Core;


use Illuminate\Support\Str;
use Skinn\Ip\Components\Core\Facades\ComponentCollectionMapper;
use Skinn\Ip\Components\Core\Facades\ComponentScriptContainer;

abstract class Component
{
    protected $component = '';
    protected $viewPath = 'front/components';
    protected $js = [];
    protected $randId = 0;

    protected $errors = [];
    protected $showErrors  = false;

    protected $data = [];

    public function __construct()
    {
        $this->data['id'] = '';
        $this->data['images'] = [];
        $this->data['randId'] = rand(0, 10000);
    }

    public function __call($name, $arguments)
    {
        if(count($arguments) == 0){
            $this->errors[] = 'using ' . $name . '() without arguments';
            return $this;
        }

        $decorator = null;
        if(starts_with($name, 'decorate'))
            return $this->decorate($name, $arguments);

        if (!$this->has($name))
            return $this;

        $this->data[$name] = $arguments[0];

        return $this;
    }

    public function addImages($images, $index = true){
        if( ! isset($this->data['images']))
            $this->data['images'] = [];

        if( ! is_array($images))
            $images = [$images];

        if( is_array($this->data['images'])){

            if( is_bool($index))
                $this->data['images'] = ($index) ? array_merge($this->data['images'], $images): array_merge($images, $this->data['images']);
            else
                array_splice( $this->data['images'], $index, 0, $images );
        }//endif


        return $this;
    }

    public function custom($key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }

    public function data($data)
    {

        if (!property_exists($this, 'data'))
            return $this;

        foreach ($data as $key => $value)
            $this->data[$key] = $value;

        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this
     */
    protected function decorate($name, $arguments)
    {
        $name = Str::camel(substr($name, strlen('decorate')));
        $decorator = $arguments[0];

        if (!$this->has($name))
            return $this;

        // -- passing the value to decorate + the extra arguments + the component
        $this->data[$name] = $decorator($this->data[$name], array_splice($arguments, 1), $this);

        return $this;
    }

    protected function doRender()
    {
    }

    public function get($component)
    {
        $this->component = $component;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    protected function has($name){
        if(isset($this->data[$name]))
            return true;

        $this->errors[] = $name . '() not found';
        return false;
    }

    public function mapCollection($property, $collection, $mapping, $merge = false)
    {

        $key = key($collection);
        if(!is_numeric($key))
            throw new \Exception('The map method excepts a collection only');

        $result = ComponentCollectionMapper::map($collection, $mapping);

        if( ! $merge)
            return $this->custom($property, $result);

        $merged = [];
        foreach($result as $index => $record)
            foreach($record as $field => $value) {
                if(! isset($merged[$field]))
                    $merged[$field] = [];

                if (is_array($value))
                    $merged[$field] = array_merge($merged[$field], $value);
                else
                    $merged[$field][] = $value;
            }//endforeach

        if(count($mapping) == 1 && is_numeric(key($mapping)))
            return $this->data([$property => reset($merged)]);

        return $this->custom($property, $merged);
    }


    public function mapImages($data, $field){
        $key = key($data);
        if(!is_numeric($key))
            return $this->mapRecord($data, ['images' => $field]);

        return $this->mapCollection('images', $data, [$field], true);
    }

    public function mapRecord($record, $mapping, $property = '')
    {
        $key = key($record);
        if(is_numeric($key))
            throw new \Exception('The map method excepts a record only');

        $result = ComponentCollectionMapper::map($record, $mapping);

        if( ! $property)
            return $this->data($result);

        return $this->custom($property, $result);
    }

    protected function registerJs($js)
    {
        ComponentScriptContainer::registerJs($js);
    }

    public function render()
    {
        $this->doRender();

        foreach($this->data as $key => $value)
            if(!$value && $value !== false && ! in_array($key, ['images']))
                $this->errors[] = $key . ' is empty';


        return $this->view();
    }

    public function showErrors($value = true){
        $this->showErrors = $value;

        return $this;
    }

    protected function view()
    {
        $view = $this->viewPath . '.' . $this->type . '.' . $this->component . '.component';
        $this->data['errors'] = $this->errors;
        $this->data['showErrors'] = $this->showErrors;

        return view($view, $this->data);
    }

    public function export() {
        return json_encode($this->data);
    }

    public function import($data)
    {
        if(is_array($data)) {
            $this->data = $data;
            return $this;
        }//endif

        $data = json_decode($data);

        if($data)
            $this->data = (array) $data;

        return $this;
    }

}