<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 30/05/2017
 * Time: 14:54
 */

namespace Skinn\Ip\Components\Maps;


use Skinn\Ip\Components\Core\Component;

class MapComponent extends Component
{
    protected $type = 'maps';

    protected $data = [
        'zoom' => 18,
        'fit' => false,
        'locations' => [],
        'key' => '',
        'center' => []
    ];

//    private $mapsJs = 'https://maps.googleapis.com/maps/api/js?key={key}&callback=initMap';

//    protected  function doRender(){
//
////        $js = $this->mapsJs;
////        $js = str_replace('{key}', $this->data['key'], $js);
////        $this->registerJs( '<script>window.initMap();</script>');
////        $this->registerJs( '<script async defer src="' . $js .'"></script>');
//    }

}