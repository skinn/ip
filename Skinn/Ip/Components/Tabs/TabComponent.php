<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 30/05/2017
 * Time: 14:54
 */

namespace Skinn\Ip\Components\Tabs;


use Skinn\Ip\Components\Core\Component;

class TabComponent extends Component
{
    protected $type = 'tabs';

    protected $data = [
        'tabs' => [
            [
                'label' => '',
                'view' => '',
                'data' => []
            ]
        ],
    ];

    
}