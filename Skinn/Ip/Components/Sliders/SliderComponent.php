<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 06/06/2017
 * Time: 09:03
 */

namespace Skinn\Ip\Components\Sliders;


use Skinn\Ip\Components\Core\Component;

class SliderComponent extends Component
{
    protected $type = 'sliders';

    protected $data = [
        'pagination' => true,
        'navigation' => true,
        'scrollbar' => false,
        'effect' => 'slide'
    ];


}
