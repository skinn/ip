<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 30/05/2017
 * Time: 14:54
 */

namespace Skinn\Ip\Components\Heroes;


use Skinn\Ip\Components\Core\Component;

class HeroComponent extends Component
{
    protected $type = 'heroes';

    protected $data = [
        'image' => '',
        'title' => '',
        'subTitle' => '',
        'cta' => '',
        'url' => '',
    ];

    // -- override method
    public function cta($value, $url = false){
        $this->data['cta'] = $value;
        $this->data['url'] = ($url) ? $url : $this->data['url'];

        return $this;
    }
}