<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 30/05/2017
 * Time: 14:54
 */

namespace Skinn\Ip\Components\MobileMenus;


use Skinn\Ip\Components\Core\Component;

class MobileMenuComponent extends Component
{
    protected $type = 'mobileMenu';

    protected $data = [
        'logo' => '',
        'items' => []
    ];


}
