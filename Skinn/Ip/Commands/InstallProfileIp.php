<?php

namespace Skinn\Ip\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;

class InstallProfileIp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ip:install:profile {profile} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'artisan ip:install:profile {profile} {--force} e.g. artisan ip:install:profile splash.default';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $profile = $this->argument('profile');
        $this->installProfile($profile);
    }

    /**
     * @param $profile
     */
    private function installProfile($profile)
    {
        $segments = explode('.', $profile);
        if( ! count($segments))
            return;

        $dir = base_path('vendor/skinn/ip/install-profiles/' . $segments[0] . '/' . $segments[1]);
        if( ! File::exists($dir))
            return;

        if(!isset($segments[1]))
            $segments[1] = 'default';

        $this->copy($dir, $segments);

        shell_exec('composer dumpautoload -d ' . base_path());
        Artisan::call('db:seed', ['--class' => 'IpSeeder']);
    }

    /**
     * @param $dir
     * @param $segments
     */
    private function copy($dir, $segments)
    {
        // Copy assets
        foreach(['js', 'sass', 'views'] as $type) {
            $target = $this->createTargetFolder($type, $segments);

            $copy = $this->option('force') ? true : false;

            if (!File::exists($target)) {
                $this->createTargetFolder($type, $segments, true);
                $copy = true;
            }//endif

            if ($copy) {
                $profileDir = $dir . '/' . $type;
                if (File::exists($profileDir))
                    File::copyDirectory($profileDir, $target);
                    $this->info('Copied ' . $type . ' to ' . $target );
            }//endif
        }//endforeach

        // Copy seeders.
        $configDir = $dir . '/config';
        $configFiles = File::allFiles($configDir);
        $targetDir = base_path('database/seeds');
        $copy = $this->option('force') ? true : false;

        foreach($configFiles as $record)
            if($copy || ! File::exists($targetDir . '/' . $record->getFileName())) {
                File::copy($configDir . '/' . $record->getFileName(), $targetDir . '/' . $record->getFileName());
                $this->info('Copied ' . $record->getFileName() . ' to ' . $targetDir);
            }
}

    /**
     * @param $type
     * @param $segments
     * @return string
     */
    private function createTargetFolder($type, $segments, $create = false)
    {
        if(in_array($type, ['js'])) {
            $targetType = 'assets/' . $type . '/vendor/skinn/ip/profiles';
        } else if (in_array($type, ['sass'])) {
            $targetType = 'assets/' . $type . '/site/vendor/ip/profiles';
        } else if (in_array($type, ['views'])) {
            $targetType = $type . '/front/partials/ip/profiles';
        } else {
            $targetType = $type;
        }
        $target = base_path('resources/' . $targetType . '/' . $segments[0] . '_' . $segments[1]);

        if ($create && !File::exists($target))
            File::makeDirectory($target, 0755, true);

        return $target;
    }
}
