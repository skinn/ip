<?php

namespace Skinn\Ip\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;


class InstallComponentsIp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ip:install:components {list} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'artisan ip:install:components {list} {--force} e.g. artisan ip:install:components maps.*,heroes.*';


    private $coreFiles = [ 'js' => 'ComponentHandler.js'];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $components = explode(',',$this->argument('list'));
        foreach($components as $component)
            $this->installComponent($component);

    }

    /**
     * @param $component
     */
    private function installComponent($component)
    {
        $segments = explode('.', $component);
        if( ! count($segments))
            return;

        $dir = base_path('vendor/skinn/ip/components/' . $segments[0]);
        if( ! File::exists($dir))
            return ;

        if(count($segments) > 1 && $segments[1] != '*') {
            var_dump(base_path('vendor/skinn/ip/components/' . $segments[0] . '/' . $segments[1], File::exists(base_path('vendor/skinn/ip/components/' . $segments[0] . '/' . $segments[1]))));
            if (!File::exists(base_path('vendor/skinn/ip/components/' . $segments[0] . '/' . $segments[1])))
                return;
        }//endif


        if(count($segments) == 1 || $segments[1] == '*'){
            $this->copyAll($dir, $segments);

            return;
        }//endif

        $this->copyOne($dir, $segments);
    }

    /**
     * @param $dir
     * @param $segments
     */
    private function copyAll($dir, $segments)
    {


        foreach (File::directories($dir) as $componentDir)
            foreach(['views', 'js', 'sass'] as $type) {
                $target = $this->createTargetFolder($segments, $type);
                $this->copyCoreFiles($dir, $type, $target);

                if (File::exists($componentDir . '/' . $type)) {

                    $target = $target . '/' . basename($componentDir);
                    $copy = $this->option('force') ? true : false;
                    if (!File::exists($target)) {
                        File::makeDirectory($target);
                        $copy = true;
                    }//endif

                    if ($copy)
                        File::copyDirectory($componentDir . '/' . $type, $target);
                }//endif
            }//endforeach
    }

    /**
     * @param $dir
     * @param $segments
     */
    private function copyOne($dir, $segments)
    {
//        if (File::exists($dir . '/core.js'))

        foreach(['views', 'js', 'sass'] as $type) {
            $target = $this->createTargetFolder($segments, $type);
            $this->copyCoreFiles($dir, $type, $target);

            $target = $target . '/' . $segments[1];

            $copy = $this->option('force') ? true : false;
            var_dump('target?', $target, File::exists($target));
            if (!File::exists($target)) {
                $copy = true;
                File::makeDirectory($target);
            }//endif

            if ($copy) {


                $componentDir = $dir . '/' . $segments[1] . '/' . $type;
                var_dump('copyOne', $componentDir, File::exists($componentDir));
                if (File::exists($componentDir))
                    File::copyDirectory($componentDir, $target);
            }//endif
        }//endforeach
}

    /**
     * @param $segments
     * @param $type
     * @return string
     */
    private function createTargetFolder($segments, $type)
    {
        if(in_array($type, ['js', 'sass'])) {
            $targetType = 'assets/' . $type . '/site';
        } else if (in_array($type, ['views'])) {
            $targetType = $type . '/front';
        } else {
            $targetType = $type;
        }
        $target = base_path('resources/' . $targetType . '/components');

        if (!File::exists($target))
            File::makeDirectory($target);


        if(File::exists(base_path('vendor/skinn/ip/components/core/' . $type)))
            if (!File::exists($target . '/core') || $this->option('force')) {
                if (!File::exists($target . '/core'))
                    File::makeDirectory($target . '/core');
                File::copyDirectory(base_path('vendor/skinn/ip/components/core/' . $type), $target . '/core');
            }//endif

        $target = $target . '/' . $segments[0];
        if (!File::exists($target))
            File::makeDirectory($target);

        return $target;
    }

    /**
     * @param $dir
     * @param $type
     * @param $target
     */
    private function copyCoreFiles($dir, $type, $target)
    {
        if (isset($this->coreFiles[$type]) && File::exists($dir . '/' . $this->coreFiles[$type]))
            File::copy($dir . '/' . $this->coreFiles[$type], $target . '/' . $this->coreFiles[$type]);
    }

}
