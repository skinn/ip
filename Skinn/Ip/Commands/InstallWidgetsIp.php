<?php

namespace Skinn\Ip\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;

class InstallWidgetsIp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ip:install:widgets {list} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'artisan ip:install:widgets {list} {--force} e.g. artisan ip:install:widgets forms.default,forms.line';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $widgets = explode(',',$this->argument('list'));
        foreach($widgets as $widget)
            $this->installWidget($widget);
    }

    /**
     * @param $widget
     */
    private function installWidget($widget)
    {
        $segments = explode('.', $widget);
        if( ! count($segments))
            return;

        $dir = base_path('vendor/skinn/ip/widgets/' . $segments[0]);
        if( ! File::exists($dir))
            return ;

        if(count($segments) > 1 && $segments[1] != '*') {
            var_dump(base_path('vendor/skinn/ip/widgets/' . $segments[0] . '/' . $segments[1], File::exists(base_path('vendor/skinn/ip/widgets/' . $segments[0] . '/' . $segments[1]))));
            if (!File::exists(base_path('vendor/skinn/ip/widgets/' . $segments[0] . '/' . $segments[1]))) {
                return;
            }
        }//endif

        if(count($segments) == 1 || $segments[1] == '*'){
            $this->copyAll($dir, $segments);

            return;
        }//endif

        $this->copyOne($dir, $segments);
    }

    /**
     * @param $dir
     * @param $segments
     */
    private function copyAll($dir, $segments)
    {
        foreach (File::directories($dir) as $widgetDir)
            foreach(['views', 'js', 'sass'] as $type) {
                $target = $this->createTargetFolder($segments, $type);

                if (File::exists($widgetDir . '/' . $type)) {

                    $target = $target . '/' . basename($widgetDir);
                    $copy = $this->option('force') ? true : false;
                    if (!File::exists($target)) {
                        File::makeDirectory($target);
                        $copy = true;
                    }//endif

                    if ($copy)
                        File::copyDirectory($widgetDir . '/' . $type, $target);
                }//endif
            }//endforeach
    }

    /**
     * @param $dir
     * @param $segments
     */
    private function copyOne($dir, $segments)
    {
        foreach(['js', 'sass', 'views'] as $type) {
            $target = $this->createTargetFolder($segments, $type);

            $target = $target . '/' . $segments[1];

            $copy = $this->option('force') ? true : false;
            var_dump('target?', $target, File::exists($target));
            if (!File::exists($target)) {
                $copy = true;
                File::makeDirectory($target);
            }//endif

            if ($copy) {
                $widgetDir = $dir . '/' . $segments[1] . '/' . $type;
                var_dump('copyOne', $widgetDir, File::exists($widgetDir));
                if (File::exists($widgetDir))
                    File::copyDirectory($widgetDir, $target);
            }//endif
        }//endforeach
    }

    /**
     * @param $segments
     * @param $type
     * @return string
     */
    private function createTargetFolder($segments, $type)
    {
        if(in_array($type, ['js'])) {
            $targetType = 'assets/' . $type . '/site/vendor/ip';
        } else if (in_array($type, ['sass'])) {
            $targetType = 'assets/' . $type . '/site/vendor/ip';
        } else if (in_array($type, ['views'])) {
            $targetType = $type . '/widgets/vendor/ip';
        } else {
            $targetType = $type;
        }

        $target = base_path('resources/' . $targetType . '/' . $segments[0]);

        if (!File::exists($target))
            File::makeDirectory($target, 0755, true);

        return $target;
    }
}
