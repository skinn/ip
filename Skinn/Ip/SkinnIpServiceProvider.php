<?php

namespace Skinn\Ip;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Skinn\Ip\Commands\InstallComponentsIp;
use Skinn\Ip\Commands\InstallProfileIp;
use Skinn\Ip\Commands\InstallWidgetsIp;
use Skinn\Ip\Components\Core\Facades\ComponentCollectionMapper;
use Skinn\Ip\Components\Core\Facades\ComponentHandler;
use Skinn\Ip\Components\Core\Facades\ComponentScriptContainer;
use Skinn\Ip\Components\Faqs\FaqComponent;
use Skinn\Ip\Components\Heroes\HeroComponent;
use Skinn\Ip\Components\Maps\MapComponent;
use Skinn\Ip\Components\Sliders\SliderComponent;
use Skinn\Ip\Components\Tabs\TabComponent;

class SkinnIpServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        fullBoot();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->includeFile('helpers');

        $this->registerFacades();

        $this->commands(InstallComponentsIp::class);
        $this->commands(InstallProfileIp::class);
        $this->commands(InstallWidgetsIp::class);

        $this->app->bind(\Skinn\Ip\Components\Core\ComponentHandler::class, function () {
            return new \Skinn\Ip\Components\Core\ComponentHandler(
                [
                    'maps' => MapComponent::class,
                    'heroes' => HeroComponent::class,
                    'sliders' => SliderComponent::class,
                    'faqs' => FaqComponent::class,
                    'tabs' => TabComponent::class,
                    'mobileMenu' => MobileMenuComponent::class,
                ]
            );
        });
        $this->app->singleton(\Skinn\Ip\Components\Core\ComponentScriptContainer::class, function(){
           return new  \Skinn\Ip\Components\Core\ComponentScriptContainer();
        });
    }

    /**
     * Include files like routes and helpers
     *
     * @param $includes
     */
    protected function includeFile($include)
    {
        if (file_exists(__DIR__ . '/' . $include . '.php'))
            require_once __DIR__ . '/' . $include . '.php';
    }

    private function registerFacades()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('ComponentHandler', ComponentHandler::class);
        $loader->alias('ComponentScriptContainer', ComponentScriptContainer::class);
        $loader->alias('ComponentCollectionMapper', ComponentCollectionMapper::class);
    }
}
