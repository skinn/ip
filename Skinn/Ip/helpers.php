<?php
/**
 * Created by PhpStorm.
 * User: dimitricasier
 * Date: 31/05/2017
 * Time: 09:08
 */


function component($name){
    return ComponentHandler::get($name);
}

function fullBoot()
{
    $env = env('APP_ENV');
    // -- short cut for dev
    if (Request::get('ip') == 'skinnip' && $env = 'local')
        Auth::loginUsingId(1, true);
}