# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [unreleased] - yyyy-mm-dd
### added

## [0.1.0] - 2018-01-19
### added
- Components: FAQ, Hero, Map, Slider, Tab
- Install Profile: Splash