<div id='{{$elementId}}' class='c-db s-form s-form--float'>
    <div v-show='!showMessage'>

        <div class="row row--wide">

            <div class="col col--12">
                <div class="s-form-group">
                    <input class="s-form__input" :class="{ 's-form__input--error' : hasError('first_name') }" id="first_name" name="first_name" type="text" v-model="model.first_name">
                    <label class="s-form__label" for="first_name">{{ trans('labels.labels.first-name') }}</label>
                    <div class="s-form__msg" v-if="hasError('first_name')" v-cloak>@{{ errors.first_name }}</div>
                </div>
            </div>
            <div class="col col--12">
                <div class="s-form-group">
                    <input class="s-form__input" :class="{ 's-form__input--error' : hasError('last_name') }" id="last_name" name="last_name" type="text" v-model="model.last_name">
                    <label class="s-form__label" for="last_name">{{ trans('labels.labels.last-name') }}</label>
                    <div class="s-form__msg" v-if="hasError('last_name')" v-cloak>@{{ errors.last_name }}</div>
                </div>
            </div>
            <div class="col col--12">
                <div class="s-form-group">
                    <input class="s-form__input" :class="{ 's-form__input--error' : hasError('email') }" id="email" name="email" type="email" v-model="model.email">
                    <label class="s-form__label" for="email">{{ trans('labels.labels.email') }}</label>
                    <div class="s-form__msg" v-if="hasError('email')" v-cloak>@{{ errors.email }}</div>
                </div>
            </div>
            <div class="col col--12">
                <div class="s-form-group">
                    <textarea class="s-form__input s-form__textarea" :class="{ 's-form__input--error' : hasError('message') }" id="message" name="message" v-model="model.message"></textarea>
                    <label class="s-form__label" for="message">{{ trans('labels.labels.question') }}</label>
                    <div class="s-form__msg" v-if="hasError('message')" v-cloak>@{{ errors.message }}</div>
                </div>
            </div>

        </div>

        <button class="s-btn" v-on:click='onSubmitForm'>{{ trans('labels.labels.send') }}</button>
        <input id="{{'thx-page-' . $elementId}}" type="hidden" value="{{ page_url('thanks')}}"/>
    </div>
    <div v-if='showMessage'>

    </div>

</div>

<script>
    var inputs = document.querySelectorAll('.s-form__input');

    forEach(inputs, function(i,v) {
        v.addEventListener('focus', function() {
            var label = this.nextElementSibling;
            label.classList.add('s-form__label--active');
        });
        v.addEventListener('blur', function() {
            if(!this.value) {
                var label = this.nextElementSibling;
                label.classList.remove('s-form__label--active');
            }
        });
    });

    function forEach(array, callback, scope) {
        for (let i = 0; i < array.length; i++) {
            callback.call(scope, i, array[i]);
        }
    }
</script>
