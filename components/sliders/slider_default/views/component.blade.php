<div id="{{$id}}" class="s-slider s-slider--default">
    @if($images)
        @if(is_array($images))
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    @foreach($images as $image)
                        <div class="swiper-slide"><img src="{{ $image }}" alt=""></div>
                    @endforeach
                </div>
                @if($pagination)
                    <div class="swiper-pagination"></div>
                @endif

                @if($navigation)
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                @endif

                @if($scrollbar)
                    <div class="swiper-scrollbar"></div>
                @endif
            </div>
        @endif
    @endif
</div>
@include('front.components.core.component')
<script>
    document.addEventListener("DOMContentLoaded", function() {
        FrontApp.components.sliders.slider_default.create('{{ $id }}','{{ $pagination }}', '{{ $navigation }}', '{{ $scrollbar }}', '{{ $effect }}');
    });
</script>
