import BaseComponent from 'site/components/core/BaseComponent'
import Swiper from 'swiper/dist/js/swiper.js'

export default class Component extends BaseComponent{
    constructor(){
        super('sliders', 'slider_default')
    }

    create(id, pag, nav, scrollbar, effect) {

        let swiperOptions = {
            loop: true,
        }

        if(pag) {
            swiperOptions.pagination = '.swiper-pagination';
        }
        if(nav) {
            swiperOptions.nextButton = '.swiper-button-next';
            swiperOptions.prevButton = '.swiper-button-prev';
        }
        if(scrollbar) {
            swiperOptions.scrollbar = '.swiper-scrollbar';
        }
        if(effect) {
            swiperOptions.effect = effect;
        }

        let mySwiper = new Swiper('.swiper-container', swiperOptions);
    }
}
