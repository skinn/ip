import BaseComponentHandler from 'site/components/core/BaseComponentHandler'

export default class ComponentHandler extends BaseComponentHandler {

    constructor(components){
        super()
        this.components = components
    }
}
