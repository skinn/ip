<div id="{{$id}}" class="s-faq s-faq--default">
    @if($questions)
        <dl class="s-faq-list">
            @foreach($questions as $index => $question)
                <div class="s-faq-element js-faq-element">
                    <dt class="c-db s-faq__question js-faq-question">
                    <div>{{ $question['question'] }}</div>
                    <div>
                        <span class="c-db s-faq__icon"></span>
                    </div>
                    </dt>
                    <dd class="s-faq__answer js-faq-answer">
                        <div class="inner">
                            {!! $question['answer'] !!}
                        </div>
                    </dd>
                </div>
            @endforeach
        </dl>
    @endif
    @include('front.components.core.component')
</div>
@section('js')
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            FrontApp.components.faqs.faq_default.create('{{ $id }}', {{ ($multiple) ? 1 : 0 }});
        })
    </script>
@append

