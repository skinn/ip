<h1>voorbeelden</h1>
<div>
    {!!
     component('faqs.faq_default')
            ->id('faq1')
            ->mapCollection('questions', $dcc->getCollection('faqs'), ['question' => 'faq.question', 'answer' => 'faq.answer'])
            ->render()
     !!}
</div>
