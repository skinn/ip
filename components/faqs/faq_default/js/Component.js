import BaseComponent from 'site/components/core/BaseComponent'
import {forEach} from 'vendor/skinn/core/Helpers'

export default class Component extends BaseComponent {
    constructor() {
        super('faqs', 'faq_default')

        this.options = {
            multiple: false
        }
    }

    create(id, multiple) {
        this.options.multiple = multiple;

        let els = document.querySelectorAll('.js-faq-element');
        forEach(els, (i, v) => {
            let q = v.querySelector('.js-faq-question');

            q.addEventListener('click', () => {
                if (v.classList.contains('is-expanded'))
                    this.closeQuestion(v);
                else
                    this.openQuestion(v);

            });
        });
    }

    openQuestion(e, t = 0.35) {
        if (!this.options.multiple)
            this.closeAllQuestions();

        e.classList.toggle('is-expanded');

        let a = e.querySelector('.js-faq-answer');
        TweenMax.set(a, {height: 'auto'});
        TweenMax.from(a, t, {height: '0', ease: Expo.easeInOut});
    }

    closeQuestion(e, t = 0.35) {
        e.classList.toggle('is-expanded');

        let a = e.querySelector('.js-faq-answer');
        TweenMax.to(a, t, {height: '0', ease: Expo.easeInOut});
    }

    closeAllQuestions() {
        let e = document.querySelector('.js-faq-element.is-expanded');
        if (e) this.closeQuestion(e);
    }
}
