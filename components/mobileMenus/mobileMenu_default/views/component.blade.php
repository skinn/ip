<div id="{{$id}}" class="s-mobile-menu s-mobile-menu--default js-mobile-menu">
    <div class="row">
        <div class="col col--3">
            @if($logo)
                <a href="" class="logo">{!! $logo !!}</a>
            @eindif
        </div>
        <div class="col col--9">
            <div class="c-ab s-hamburger js-hamburger">

            </div>
        </div>
    </div>
    @include('front.components.core.component')

</div>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        FrontApp.components.mobileMenu.mobileMenu_default.create('{{ $id }}')
    })
</script>
