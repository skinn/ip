import MobileMenuComponent from 'site/components/mobileMenu/component/MobileMenuComponent'
import {forEach} from 'vendor/skinn/core/Helpers'

export default class Component extends MobileMenuComponent{
    constructor(){
        super('mobileMenu', 'mobileMenu_default')

        this.mobileBtn;
    }

   	create(id, tabs) {
        console.log('create van mobilecomponent')
        this.mobileBtn = document.querySelector('.js-hamburger');
        this.mobileBtn.addEventListener('click', () => {
            if(this.mobileBtn.classList.contains('is-open'))
                this.closeMobileMenu();
            else
                this.openMobileMenu();
        });
    }

    openMobileMenu(e, t = 0.35) {
        if(!this.options.multiple)
            this.closeAllQuestions();

        this.mobileBtn.classList.toggle('is-open');

        let a = this.mobileBtn.querySelector('.js-mobile-menu');
        TweenMax.set(a, { height: 'auto' });
        TweenMax.from(a, t, { height: '0', ease: Expo.easeInOut });
    }

    closeMobileMenu(e, t = 0.35) {
        this.mobileBtn.classList.toggle('is-open');

        let a = this.mobileBtn.querySelector('.s-faq__answer');
        TweenMax.to(a, t, { height: '0', ease: Expo.easeInOut });
    }
}
