import BaseComponent from 'site/components/core/BaseComponent'
import {forEach} from 'vendor/skinn/core/Helpers'

export default class Component extends BaseComponent{
    constructor(type, name){
        super(type, name)
    }

}

