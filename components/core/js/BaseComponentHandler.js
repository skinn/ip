/**
 * Created by dimitricasier on 01/06/2017.
 */
export default class BaseComponentHandler {
    constructor(type, name){
        this._type = type
        this._name = name
    }

    init(){
        // -- loops init components
        for(let name in this.components){
            let component = new this.components[name]()
            component.init()
        }//endfor
    }
}