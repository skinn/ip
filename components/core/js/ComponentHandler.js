/*
import ComponentHandler from 'components/core/ComponentHandler'


// -------------------
// -- initClasses() --
// -------------------

...
this.componentHandler = new ComponentHandler(
    {
        'maps': {
            'handler': require('components/maps/ComponentHandler').default,
            'components': {
                'map1': require('components/maps/map1/Component').default
            }
        }
    });


// ------------
// -- init() --
// ------------

...
this.componentHandler.init();

*/

export default class ComponentHandler {
    constructor(settings) {
        this.settings = settings
    }

    init() {
        this.initWindow();
        this.initHandlers();
    }

    initWindow() {
        if (window.FrontApp.components === undefined)
            window.FrontApp.components = {}
    }

    initHandlers() {
        for (let type in this.settings) {
            let handler = new this.settings[type].handler(this.settings[type].components)
            handler.init()
        }//endfor
    }
}
