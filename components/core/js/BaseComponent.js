/**
 * Created by dimitricasier on 01/06/2017.
 */
export default class BaseComponent {
    constructor(type, name){
        this._type = type
        this._name = name
        this._id = null;
    }

    init() {
        if (window.FrontApp.components[this._type] === undefined)
            window.FrontApp.components[this._type] = {}

        window.FrontApp.components[this._type][this._name] = this

        if(FrontApp.components.instances === undefined)
            FrontApp.components.instances = {}

        if(FrontApp.components.instances[this._type] === undefined)
            FrontApp.components.instances[this._type] = {}
    }

    listItem(list, key, defaultVal = ''){
        let keys = key.split('.')
        if( ! keys.length)
            return defaultVal

        let segment = keys[0]
        if(list[segment] === undefined)
            return defaultVal


        if(keys.length == 1)
            return list[segment]

        return this.listItem(list[segment], keys.shift(), defaultVal)
    }

    querySelectorHelper(query) {
        if(!this._id) {
            console.error('id for component not defined');
        }
        return document.querySelectorAll('#' + this._id + ' ' + query);
    }
}
