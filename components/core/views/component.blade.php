@if($errors && $showErrors)
    <style>
        .error {
            background: rgba(204, 0, 0, 0.8);
            color: #fff;
            padding: 10px;
            font-size: 18px;
            z-index: 3000;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            border: 1px solid #fff;
        }
    </style>
    <div class="error" style="">

        <div class="errors">
            @foreach($errors as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    </div>
@endif
