import BaseComponent from 'site/components/core/BaseComponent'
import GoogleMaps from 'site/components/maps/core/GoogleMaps'


export default class Component extends BaseComponent{
    constructor(){
        super('maps', 'map_default')
    }

    create(id, locations, options, key = '') {

        let map = new GoogleMaps(id, key)
        map.hasInfoWindows(true)

        // Override styles
        // TODO: refactor
        options.styles = this.getStyling();

        map.render(options, locations)

        FrontApp.components.instances.maps[id] = { map }
    }

    getStyling() {
        return [];
    }


}

