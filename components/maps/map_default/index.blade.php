<h1>voobeelden</h1>
<div>
    {!!
            component('maps.map_default')
            ->id('map46')
            ->zoom(8)
            ->fit(true)
            ->key('AIzaSyAgJ9YYUkfwT9g6b3ab0aXDf0bx00M_alg')
            ->center(['lat' => 51.033255, 'lng' => 3.7113564])
            ->showErrors(false)
            ->render()
    !!}
    <button id="clickmap46">add</button>
    <button id="clickmap46r">reset</button>
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            document.getElementById('clickmap46').addEventListener('click', function () {
                return function () {
                    FrontApp.components.instances.maps['map46'].map.addLocation({
                        'icon': 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png',
                        'text': 'gent',
                        'lat': 51.033255 + Math.random(),
                        'lng': 3.711356 + Math.random()
                    }, true);

                }();
            }, false)

            document.getElementById('clickmap46r').addEventListener('click', function () {
                return function () {
                    FrontApp.components.instances.maps['map46'].map.reset();

                }();
            }, false)
        });
    </script>
</div>

<div>
    {!!
            component('maps.map_default')
            ->id('map45')
            ->zoom(10)
            ->center(['lat' => 51.033255, 'lng' => 3.7113564])
            ->key('AIzaSyAgJ9YYUkfwT9g6b3ab0aXDf0bx00M_alg')
            ->locations([
                [
                    'icon' => 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png',
                    'text' => 'gent',
                    'lat' => 51.033255,
                    'lng' => 3.711356
                ],
                [
                    'text' => 'danehill',
                    'lat' => 51.053255,
                    'lng' => 3.811356]
                ])
                ->showErrors(true)
                ->render()
            !!}

</div>
<div>
    {!!

        component('maps.map_default')
        ->id('map456')
        ->mapRecord($dcc->getFirstFromCollection('companies'), ['text' => 'companies.name','lat' => 'companies.latitude', 'lng' => 'companies.longitude'],'center')
        ->fit(true)
        ->key('AIzaSyAgJ9YYUkfwT9g6b3ab0aXDf0bx00M_alg')
        ->mapCollection('locations', $dcc->getCollection('companies'), ['text' => 'companies.name', 'lat' => 'companies.latitude', 'lng' => 'companies.longitude'])
        ->render()
        !!}
</div>
