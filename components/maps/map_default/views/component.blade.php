<div id="{{ $id }}" class="s-map s-map--default"></div>
@include('front.components.core.component')
<script>
    document.addEventListener("DOMContentLoaded", function() {
        FrontApp.components.maps.map_default.create('{{ $id }}', {!! json_encode($locations) !!}, {
                'center': {!! json_encode($center) !!},
                'zoom': {{ $zoom }},
                'fit': {{ ($fit) ? 1 : 0 }}
            },
            '{{ $key }}'
        )
    })
</script>
