let GoogleMapsLoader = require('google-maps')

export default class GoogleMaps {
    constructor(id, key) {
        this.bounds = null
        this.id = id
        this.key = key
        this.locations = []
        this.map = null
        this.marker = null
        this.markers = []
        this.useInfoWindows = false
        this.timeout = null
        this.options = null
    }

    addLocation(location, isCenter = false) {
        if( ! this.key)
            return this

        GoogleMapsLoader.KEY = this.key

        this.addMarker(location, isCenter);
    }

    addMarker(location, isCenter = false) {
        this.locations.push(location)
        GoogleMapsLoader.load((google) => {

            if(isCenter)
                this.map.setCenter(new google.maps.LatLng(location.lat, location.lng))


            if (this.marker) {
                this.marker.setAnimation(null)
            }

            // new marker
            this.marker = new google.maps.Marker({
                position: new google.maps.LatLng(location.lat, location.lng)
            })

            // -- info balloons
            if (this.useInfoWindows) {
                let infowindow = new google.maps.InfoWindow()
                google.maps.event.addListener(this.marker, 'click', ((marker, i) => {
                    return () => {
                        infowindow.setContent(location.text + ' ' + location.lat + ' ' + ', ' + ' ' + location.lng)
                        infowindow.open(this.map, marker)
                    }
                })(this.marker, this.locations.length - 1))
            }// endif

            // put marker on map
            this.marker.setMap(this.map)

            this.marker.setAnimation(google.maps.Animation.BOUNCE)

            if (this.timeout) {
                window.clearTimeout(this.timeout)
            }

            this.timeout = setTimeout(() => {
                this.marker.setAnimation(null)
            }, 1400)

            this.bounds.extend(this.marker.position)
            this.fitBounds();

            this.markers.push(this.marker)
        })
    }

    fitBounds() {
        if (this.options && this.options.fit && this.locations.length > 1) {
            this.map.fitBounds(this.bounds)

            return
        }// endif

        if (this.options && this.options.fit && !this.options.center) {

        }
    }

    openInfoWindow(id) {
        GoogleMapsLoader.load((google) => {
            google.maps.event.trigger(this.map.markers[id], 'click')
        })
    }

    render(options, locations) {
        if( ! this.key)
            return this

        GoogleMapsLoader.KEY = this.key

        this.setOptions(options);
        this.addMultipleMarkers(locations);
    }

    setOptions(options) {
        this.options = options

        if (this.options.center) {
            this.options.center.lat = parseFloat(this.options.center.lat)
            this.options.center.lng = parseFloat(this.options.center.lng)
        }// endif
    }

    addMultipleMarkers(locations) {
        this.locations = JSON.parse(JSON.stringify(locations))
        GoogleMapsLoader.load((google) => {
            if (this.map === null) {
                this.map = new google.maps.Map(document.getElementById(this.id), this.options)
            }// endif

            if (this.map && this.map.markers !== undefined && this.map.markers.length > 0) {
                for (let i = 0; i < this.map.markers.length; i++) {
                    this.map.markers[i].setMap(null)
                    delete this.map.markers[i]
                }// endfor
            }// endif

            if (this.locations === undefined) {
                this.locations = []
            }// endif

            // -- markers
            this.bounds = new google.maps.LatLngBounds()
            for (let i = 0; i < this.locations.length; i++) {
                let markerOptions = {
                    position: new google.maps.LatLng(this.locations[i].lat, this.locations[i].lng),
                    map: this.map
                }

                if (this.locations[i].icon) {
                    markerOptions.icon = this.locations[i].icon
                }// endif

                this.marker = new google.maps.Marker(markerOptions)

                this.bounds.extend(this.marker.position)

                // -- info balloons
                if (this.useInfoWindows) {
                    let infowindow = new google.maps.InfoWindow()
                    google.maps.event.addListener(this.marker, 'click', ((marker, i) => {
                        return () => {
                            infowindow.setContent(this.locations[i].text + ' ' + this.locations[i].lat + ' ' + ', ' + ' ' + this.locations[i].lng)
                            infowindow.open(this.map, marker)
                        }
                    })(this.marker, i))
                }// endif
            }// endfor


            this.fitBounds()

            this.markers.push(this.marker)
        })
    }

    reset(){
        for (var i = 0; i < this.markers.length; i++) {
            if(this.markers[i])
                this.markers[i].setMap(null)
        }//endfor

        this.markers = []
    }

    hasInfoWindows(value) {
        this.useInfoWindows = value
    }
}
