<div id="{{$id}}" class="s-hero s-hero--default">
    <img class="s-hero__image lazyload" data-srcset="{{$image}}" alt="{{ strip_tags($title) }}" />
    @if($title || $subTitle || $url)
        <div class="c-abf s-hero-content">
            @if($title)
                <div class="s-hero__title">{!! $title !!}</div>
            @endif
            @if($subTitle)
                <div class="s-hero__subtitle">{!! $subTitle !!}</div>
            @endif
            @if($url && $cta)
                <div class="s-hero__cta">
                    <a class="button" href="{{ $url }}">{!! $cta !!}</a>
                </div>
            @endif
        </div>
    @endif
    @include('front.components.core.component')
</div>
