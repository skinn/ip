import BaseComponent from 'site/components/core/BaseComponent'
import LazySizes from 'lazysizes'
import ObjectFit from 'lazysizes/plugins/object-fit/ls.object-fit.min.js';
import PictureFill from 'picturefill';
import Intrinsic from 'picturefill/src/plugins/intrinsic-dimension/pf.intrinsic.js';

export default class Component extends BaseComponent{
    constructor(){
        super('heroes', 'hero_default')
    }
}
