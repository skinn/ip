import BaseComponent from 'site/components/core/BaseComponent'
import {forEach} from 'vendor/skinn/core/Helpers'

export default class Component extends BaseComponent {
    constructor(type, name) {
        super(type, name)

        this._animationSpeed = 0.35;
    }

    create(id, tabs) {
        this._id = id;

        let tabButtons = this.querySelectorHelper('.js-tab-button');
        forEach(tabButtons, (index, button) => {
            if (index == 0) {
                this.setActiveButton(button);
                this.setActiveTab(index);
            }
            button.addEventListener('click', (e) => {
                let el = e.currentTarget;
                this.setActiveButton(el);
                let tabId = el.getAttribute('data-tabindex');
                this.setActiveTab(tabId);
            })
        });
    }

    setActiveButton(targetEl) {
        let tabButtons = this.querySelectorHelper('.js-tab-button');
        forEach(tabButtons, (index, button) => {
            if (button != targetEl)
                button.classList.remove('is-active');
            else
                button.classList.add('is-active');
        });
    }

    setActiveTab(tabId) {
        let tabViews = this.querySelectorHelper("*[class*='js-tab-content-']");
        forEach(tabViews, (index, view) => {
            if (view.classList.contains('js-tab-content-' + tabId)) {
                view.style.display = "block";
                TweenMax.to(view, this._animationSpeed, {opacity: 1, ease: Expo.easeInOut});
            } else {
                TweenMax.killTweensOf(view);
                view.style.display = "none";
                view.style.opacity = 0;
            }
        });
    }
}

