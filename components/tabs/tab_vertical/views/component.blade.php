<div id="{{$id}}" class="s-tab s-tab--vertical">
    <div class="row row--gapless">
        <ul class="col col--12 col--2-sm s-tab__head">
            @foreach($tabs as $index => $tab)<li class="c-db">
                <button type="button" class="c-dib s-tab__button js-tab-button" data-tabindex="{{$index}}">
                    {{ $tab['label']}}
                </button>
            </li>@endforeach
        </ul>
        <div class="col col--12 col--10-sm s-tab__content">
            @foreach($tabs as $index => $tab)
                <div class="c-db s-tab__content-inner js-tab-content-{{$index}}">
                    @if(View::exists($tab['view']))
                        @include($tab['view'], ['data' => isset($tab['data']) ? $tab['data'] : []])
                    @else
                        view, {{$tab['view']}},  doesn't exists
                    @endif
                </div>

            @endforeach
        </div>
        @include('front.components.core.component')
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        FrontApp.components.tabs.tab_vertical.create('{{ $id }}', {!! json_encode($tabs) !!})
    })
</script>
